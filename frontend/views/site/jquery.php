<div class="container">
    <h1>JQuery demo</h1>
    <div class="container row">
        <div id="c1" style="width: 100px; height: 100px; background-color: #eea236; float: left">
        </div>
    </div>
    <h3>покрасить кубик</h3>
    <button class="btn" onclick="blue()" style="background-color: #2e6da4">синий</button>
    <button class="btn" onclick="red()" style="background-color: #a94442">красный</button>
    <button class="btn" onclick="green()" style="background-color: #3e8f3e">зеленый</button>
    <script>
        function blue() {
            $('#c1').css("background-color", "#2e6da4");
        };

        function red() {
            $('#c1').css("background-color", "#a94442");
        };

        function green() {
            $('#c1').css("background-color", "#3e8f3e");
        }
    </script>
    <h3>изменить размер</h3>
    <button class="btn bg-info" onclick="size_min()">min</button>
    <button class="btn bg-warning" onclick="size_med()">medium</button>
    <button class="btn bg-danger" onclick="size_max()">max</button>
    <script>
        function size_min() {
            $('#c1').css("width", "50px");
            $('#c1').css("height", "50px");
        };

        function size_med() {
            $('#c1').css("width", "100px");
            $('#c1').css("height", "100px");
        };

        function size_max() {
            $('#c1').css("width", "150px");
            $('#c1').css("height", "150px");
        }
    </script>
    <h3>добавить кубик</h3>
    <button class="btn" onclick="add()" style="background-color: ">добавить до</button>
    <button class="btn" onclick="after()" style="background-color: ">добавить после</button>
    <button class="btn" onclick="rem()" style="background-color: ">удалить серые кубики</button>
    <script>
        function size_min() {
            $('#c1').css("width", "50px");
            $('#c1').css("height", "50px");
        };

        function add() {
            $('#c1').before("<div class=\"shadow\" style=\"width: 100px; height: 100px; background-color: gray; float: left\"></div>");
        };

        function after() {
            $('#c1').after("<div class=\"shadow\" style=\"width: 100px; height: 100px; background-color: gray; float: left\"></div>");
        };

        function rem() {
            $('.shadow').remove();
        };
    </script>
    <h3>DOM</h3>
    <div class="container row dom">
        <div class="dp1" style="width: 300px; height: 300px; background-color: gray; border: white 1px solid">
            <div class="dp2" style="width: 250px; height: 250px; background-color: gray; border: white 1px solid">
                <div class="dp3"
                     style="width: 200px; height: 200px; background-color: gray; border: white 1px solid">
                    <div class="dp4"
                         style="width: 150px; height: 150px; background-color: gray; border: white 1px solid">
                        <div class="dp5"
                             style="width: 100px; height: 100px; background-color: gray; border: white 1px solid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h3>parent and child</h3>
    <button class="btn" onclick="parent()">parent</button>
    <button class="btn" onclick="child()">child</button>
    <script>
        var focus = $('.dp3');
        focus.css("background-color", "yellow");

        function parent() {
            if (focus.attr("class") != "dp1") {
                $('.dom div').css("background-color", "gray");
                focus = focus.parent();
                focus.css("background-color", "yellow");
            }
        };

        function child() {
            if (focus.attr("class") != "dp5") {
                $('.dom div').css("background-color", "gray");
                focus = focus.children();
                focus.css("background-color", "yellow");
            }
        };
    </script>
    <br><br>
    <div class="container row dom2">
        <div style="width: 100px; height: 100px; background-color: gray; float: left; border: white 1px solid"></div>
        <div style="width: 100px; height: 100px; background-color: gray; float: left; border: white 1px solid"></div>
        <div style="width: 100px; height: 100px; background-color: gray; float: left; border: white 1px solid"></div>
    </div>
    <h3>brothers</h3>
    <button class="btn" onclick="div_eq(0)">div1</button>
    <button class="btn" onclick="div_eq(1)">div2</button>
    <button class="btn" onclick="div_eq(2)">div3</button>
    <script>
        var focus2 = $(".dom2").children();
        function div_eq($eq) {
            $(".dom2").children().css("background-color", "gray");;
            focus2.eq($eq).css("background-color", "#eea236");
        };
    </script>
    <h1>Репозиторий</h1>
    <p>
        ссылка на репозиторий <a href="https://bitbucket.org/izatblv/yiidemo/src/master/frontend/views/site/jquery.php"
                                 target="_blank">https://bitbucket.org/izatblv/yiidemo/src/master/frontend/views/site/jquery.php</a>
    </p>
</div>