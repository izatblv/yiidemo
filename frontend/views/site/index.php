<?php

/* @var $this yii\web\View */

$this->title = 'Yii demo';
?>
<div class="container site-index">

    <div class="text-center"><img src="https://upload.wikimedia.org/wikipedia/ru/d/d2/Yii_logo.png" alt=""
                                  style="width: 50%"></div>
    <div>
        <h1 class="text-center">Демонстрации</h1>
        <div>
            <h2>База данных MySQL</h2>
            <a href="/pages" target="_blank">
            страницы
            </a>
            <a href="/jquery" target="_blank">
                <h2>JQuery</h2>
            </a>
        </div>
    </div>
    <div>
        <h1 class="text-center">Репозиторий</h1>
        <p class="text-center">ссылка на репозиторий <a href="https://bitbucket.org/izatblv/yiidemo" target="_blank">https://bitbucket.org/izatblv/yiidemo</a>
        </p>
    </div>

</div>
